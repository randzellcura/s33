// 3. Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos')
    .then((res) => res.json())
    .then((data) => console.log(data))

// 5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API. + 6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.

async function getData() {
    let result = await fetch('https://jsonplaceholder.typicode.com/todos/1')
    let data = await result.json()
    let { title, completed } = data
    console.log(data)
    console.log('This item ' + '"' + title + '"' + ' on the list has a status of ' + completed)
}
getData()

// 7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos', {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
        title: 'Created To Do List Item'
    })
})
    .then((res) => res.json())
    .then((data) => console.log(data))

// 8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'PUT',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
        dateCompleted: 'Pending',
        description: 'To update the my to do list with a different data structure',
        status:'Pending',
        title:'Updated To Do List Item'
    })
})
    .then((res) => res.json())
    .then((data) => console.log(data))


// 10. Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'PATCH',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
        title: 'Updated To Do List',
    })
})
    .then((res) => res.json())
    .then((data) => console.log(data))

// 11. Update a to do list item by changing the status to complete and add a date when the status was changed.
fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'PATCH',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
        completed: true,
        dateCompleted: '07/09/21',
        status: 'Complete'

    })
})
    .then((res) => res.json())
    .then((data) => console.log(data))

// 12. Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'DELETE',
})
    .then((res) => res.json())
    .then((data) => console.log(data))